﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jobAppLibrary.Repositories
{
    public interface ICrudRepository<TEntity>
    {
       
        Task<int> Add(TEntity entity);
        Task<bool> delete(int id);
        Task<TEntity> get(int id);
        Task<bool> update(TEntity entityToUpdate);

        Task<IEnumerable<TEntity>> GetAll();   
    }
}
