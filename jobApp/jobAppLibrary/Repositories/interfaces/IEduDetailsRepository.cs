﻿
namespace jobAppLibrary.Repositories
{
    public interface IEduDetailsRepository
    {
        Task<List<BasicDetail>> GetAll();
    }
}