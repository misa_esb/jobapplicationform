﻿using jobAppLibrary.Models;

namespace jobAppLibrary.Repositories
{
    public interface IbasicDetailsRepository : ICrudRepository<BasicDetailModel>
    {
        Task<int> Add(BasicDetailModel model);
        Task<bool> delete(int id);
        Task<BasicDetailModel> get(int id);
        Task<bool> update(BasicDetailModel model);
        Task<IEnumerable<BasicDetailModel>> GetAll();

    }
}