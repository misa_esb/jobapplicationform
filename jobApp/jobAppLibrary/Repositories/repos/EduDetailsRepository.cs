﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jobAppLibrary.Repositories
{
    public class EduDetailsRepository : IEduDetailsRepository
    {

        private readonly jobapplicationdbContext _context = null;

        public EduDetailsRepository(jobapplicationdbContext context)
        {
            _context = context;
        }



        public async Task<List<BasicDetail>> GetAll()
        {

            var data = _context.BasicDetails.Include(a => a.EduDetails).ToList();

            return data;


        }

    }
}

