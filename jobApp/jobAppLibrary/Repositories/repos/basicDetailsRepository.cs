﻿global using jobApp.data;
global using jobAppLibrary.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace jobAppLibrary.Repositories
{
    public class basicDetailsRepository : IbasicDetailsRepository 
    {
        private readonly jobapplicationdbContext _context = null;

        public basicDetailsRepository(jobapplicationdbContext context)
        {
            _context = context;
        }

        public async Task<int> Add(BasicDetailModel model)
        {
            var newEmployee = new BasicDetail()
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Designation = model.Designation,
                Addr1 = model.Addr1,
                Addr2 = model.Addr2,
                Email = model.Email,
                City = model.City,
                PhoneNo = model.PhoneNo,
                State = model.State,
                Gender = model.Gender,
                Zipcode = model.Zipcode,
                Relationship = model.Relationship,
                Dob = model.Dob,

            };

            newEmployee.EduDetails = new List<EduDetail>();
            foreach(var a in model.EduDetails)
            {
                newEmployee.EduDetails.Add(new EduDetail()
                {
                    CourseId = a.CourseId,
                    NameOfBoard = a.NameOfBoard,
                    NameOfUni = a.NameOfUni,
                    PassYear = a.PassYear,
                    Percentage = a.Percentage
                });
            }

            newEmployee.PreferenceContacts = new List<PreferenceContact>();
            foreach (var a in model.PreferenceContacts)
            {
                newEmployee.PreferenceContacts.Add(new PreferenceContact()
                {
                    LocId = a.LocId,
                    DeptId = a.DeptId,
                    CurrCtc = a.CurrCtc,
                    ExpCtc = a.ExpCtc,
                    NoticePeriod = a.NoticePeriod,
                });
            }

            newEmployee.ReferenceContacts = new List<ReferenceContact>();
            foreach (var a in model.ReferenceContacts)
            {
                newEmployee.ReferenceContacts.Add(new ReferenceContact()
                {
                    RefName = a.RefName,
                    RefRelation = a.RefRelation,
                    RefContact = a.RefContact,
                });
            }
            newEmployee.WorkExperiences = new List<WorkExperience>();
            foreach(var a in model.WorkExperiences)
            {
                newEmployee.WorkExperiences.Add(new WorkExperience()
                {
                    CompanyName = a.CompanyName,
                    Designation = a.Designation,
                    FromD = a.FromD,
                    ToD = a.ToD,
                });
            }

            newEmployee.LanguageMappings = new List<LanguageMapping>();
            foreach (var a in model.languages)
            {
                newEmployee.LanguageMappings.Add(new LanguageMapping()
                {
                  OptionId = a.OptionId,
                  LangName  = a.LangName,
                });
            }

            newEmployee.TechnologyMappings = new List<TechnologyMapping>();
            foreach (var a in model.techs)
            {
                newEmployee.TechnologyMappings.Add(new TechnologyMapping()
                {
                    OptionId = a.OptionId,
                    TechName = a.TechName,
                });
            }

            await _context.BasicDetails.AddAsync(newEmployee);
            await _context.SaveChangesAsync();
            return newEmployee.AppliId;
        }

        public async Task<bool> delete(int id)
        {
            var emp = await _context.BasicDetails.Include(a => a.EduDetails).Include(a => a.PreferenceContacts).Include(a=> a.LanguageMappings).Include(a=>a.TechnologyMappings).Include(a => a.ReferenceContacts).Include(a => a.WorkExperiences).FirstOrDefaultAsync(x => x.AppliId == id);
            if (emp == null)
                return false;
            else
            {
                _context.BasicDetails.Remove(emp);
                await _context.SaveChangesAsync();
                return true;
            }
        }

        public async Task<bool> update(BasicDetailModel model)
        {
            var newEmployee = new BasicDetail()
            {
                AppliId = model.AppliId,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Designation = model.Designation,
                Addr1 = model.Addr1,
                Addr2 = model.Addr2,
                Email = model.Email,
                City = model.City,
                PhoneNo = model.PhoneNo,
                State = model.State,
                Gender = model.Gender,
                Zipcode = model.Zipcode,
                Relationship = model.Relationship,
                Dob = model.Dob,

            };
            newEmployee.EduDetails = new List<EduDetail>();
            foreach (var a in model.EduDetails)
            {
                newEmployee.EduDetails.Add(new EduDetail()
                {
                    CourseId = a.CourseId,
                    NameOfBoard = a.NameOfBoard,
                    NameOfUni = a.NameOfUni,
                    PassYear = a.PassYear,
                    Percentage = a.Percentage
                });
            }

            newEmployee.PreferenceContacts = new List<PreferenceContact>();
            foreach (var a in model.PreferenceContacts)
            {
                newEmployee.PreferenceContacts.Add(new PreferenceContact()
                {
                    LocId = a.LocId,
                    DeptId = a.DeptId,
                    CurrCtc = a.CurrCtc,
                    ExpCtc = a.ExpCtc,
                    NoticePeriod = a.NoticePeriod,
                });
            }

            newEmployee.ReferenceContacts = new List<ReferenceContact>();
            foreach (var a in model.ReferenceContacts)
            {
                newEmployee.ReferenceContacts.Add(new ReferenceContact()
                {
                    RefName = a.RefName,
                    RefRelation = a.RefRelation,
                    RefContact = a.RefContact,
                });
            }

            newEmployee.WorkExperiences = new List<WorkExperience>();
            foreach (var a in model.WorkExperiences)
            {
                newEmployee.WorkExperiences.Add(new WorkExperience()
                {
                    CompanyName = a.CompanyName,
                    Designation = a.Designation,
                    FromD = a.FromD,
                    ToD = a.ToD,
                });
            }
            newEmployee.LanguageMappings = new List<LanguageMapping>();
            foreach (var a in model.languages)
            {
                newEmployee.LanguageMappings.Add(new LanguageMapping()
                {
                    OptionId = a.OptionId,
                    LangName = a.LangName,
                });
            }

            newEmployee.TechnologyMappings = new List<TechnologyMapping>();
            foreach (var a in model.techs)
            {
                newEmployee.TechnologyMappings.Add(new TechnologyMapping()
                {
                    OptionId = a.OptionId,
                    TechName = a.TechName,
                });
            }
            _context.BasicDetails.Update(newEmployee);
            await _context.SaveChangesAsync();
            return true;
        }



        public async Task<BasicDetailModel> get(int id)
        {
            var model =await _context.BasicDetails.Include(a => a.EduDetails).ThenInclude(b=>b.Course).Include(a => a.LanguageMappings).Include(a => a.TechnologyMappings).Include(a => a.PreferenceContacts).Include(a => a.ReferenceContacts).Include(a => a.WorkExperiences).FirstOrDefaultAsync(x => x.AppliId == id);

            var newEmployee = new BasicDetailModel()
            {
                AppliId = model.AppliId,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Designation = model.Designation,
                Addr1 = model.Addr1,
                Addr2 = model.Addr2,
                Email = model.Email,
                City = model.City,
                PhoneNo = model.PhoneNo,
                State = model.State,
                Gender = model.Gender,
                Zipcode = model.Zipcode,
                Relationship = model.Relationship,
                Dob = model.Dob,

                EduDetails = model.EduDetails.Select(a => new EduDetailModel()
                {
                    CourseId = a.CourseId,
                    course = a.Course,
                    NameOfBoard = a.NameOfBoard,
                    NameOfUni = a.NameOfUni,
                    PassYear = a.PassYear,
                    Percentage = a.Percentage
                }).ToList(),

                PreferenceContacts = model.PreferenceContacts.Select(a => new PreferenceContactModel()
                {
                    LocId = a.LocId,
                    DeptId = a.DeptId,
                    CurrCtc = a.CurrCtc,
                    ExpCtc = a.ExpCtc,
                    NoticePeriod = a.NoticePeriod,

                }).ToList(),
                ReferenceContacts = model.ReferenceContacts.Select(a => new ReferenceContactModel()
                {
                    RefName = a.RefName,
                    RefRelation = a.RefRelation,
                    RefContact = a.RefContact,

                }).ToList(),
                WorkExperiences = model.WorkExperiences.Select(a => new WorkExperienceModel()
                {
                    CompanyName = a.CompanyName,
                    Designation = a.Designation,
                    FromD = a.FromD,
                    ToD = a.ToD,

                }).ToList(),
                languages = model.LanguageMappings.Select(a => new LanguageMappingModel()
                {
                    LangId = a.LangId,
                    LangName = a.LangName,

                }).ToList(),
                techs = model.TechnologyMappings.Select(a => new TechnologyMappingModel()
                {
                     OptionId = a.OptionId,
                    TechName = a.TechName,
                }).ToList()
                
            };

            return newEmployee;
        }

        public async Task<IEnumerable<BasicDetailModel>> GetAll()
        {
            var empList = new List<BasicDetailModel>();

            var dataVals = _context.BasicDetails.Include(a => a.EduDetails)
                .Include(a=> a.PreferenceContacts)
                .Include(a=> a.ReferenceContacts)
                .Include(a=>a.WorkExperiences)
                .Include(a => a.LanguageMappings)
                .Include(a => a.TechnologyMappings);

            foreach (var model in dataVals)
            {
                empList.Add(new BasicDetailModel()
                {
                    AppliId = model.AppliId,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Designation = model.Designation,
                    Addr1 = model.Addr1,
                    Addr2 = model.Addr2,
                    Email = model.Email,
                    City = model.City,
                    PhoneNo = model.PhoneNo,
                    State = model.State,
                    Gender = model.Gender,
                    Zipcode = model.Zipcode,
                    Relationship = model.Relationship,
                    Dob = model.Dob,
                    
                    EduDetails = model.EduDetails.Select(a=> new EduDetailModel()
                    {
                        CourseId = a.CourseId,
                        NameOfBoard = a.NameOfBoard,
                        NameOfUni = a.NameOfUni,
                        PassYear = a.PassYear,
                        Percentage = a.Percentage
                    }).ToList(),

                    PreferenceContacts = model.PreferenceContacts.Select(a=>new PreferenceContactModel()
                    {
                        LocId = a.LocId,
                        DeptId = a.DeptId,
                        CurrCtc = a.CurrCtc,
                        ExpCtc = a.ExpCtc,
                        NoticePeriod = a.NoticePeriod,

                    }).ToList(),
                    ReferenceContacts = model.ReferenceContacts.Select(a=>new ReferenceContactModel()
                    {
                        RefName = a.RefName,
                        RefRelation = a.RefRelation,
                        RefContact = a.RefContact,

                    }).ToList(),
                    WorkExperiences = model.WorkExperiences.Select(a=>new WorkExperienceModel()
                    {
                        CompanyName = a.CompanyName,
                        Designation = a.Designation,
                        FromD = a.FromD,
                        ToD = a.ToD,

                    }).ToList(),
                     languages = model.LanguageMappings.Select(a => new LanguageMappingModel()
                     {
                         LangId = a.LangId,
                         LangName = a.LangName,

                     }).ToList(),
                    techs = model.TechnologyMappings.Select(a => new TechnologyMappingModel()
                    {
                        OptionId = a.OptionId,
                        TechName = a.TechName,
                    }).ToList()

                });
                
            }
            return empList;


        }
      
      
    }
}
