﻿using System;
using System.Collections.Generic;

namespace jobApp.data
{
    public partial class EduDetail
    {
        public int EduId { get; set; }
        public int AppliId { get; set; }
        public int? CourseId { get; set; }
        public string? NameOfBoard { get; set; }
        public string? NameOfUni { get; set; }
        public string? PassYear { get; set; }
        public int Percentage { get; set; }

        public virtual BasicDetail Appli { get; set; } = null!;
        public virtual OptionMaster? Course { get; set; }
    }
}
