﻿using System;
using System.Collections.Generic;

namespace jobApp.data
{
    public partial class WorkExperience
    {
        public int WorkId { get; set; }
        public int? AppliId { get; set; }
        public string? CompanyName { get; set; }
        public string? Designation { get; set; }
        public DateTime? FromD { get; set; }
        public DateTime? ToD { get; set; }

        public virtual BasicDetail? Appli { get; set; }
    }
}
