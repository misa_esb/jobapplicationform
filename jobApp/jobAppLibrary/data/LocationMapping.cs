﻿using System;
using System.Collections.Generic;

namespace jobApp.data
{
    public partial class LocationMapping
    {
        public int LocId { get; set; }
        public int AppliId { get; set; }
        public int OptionId { get; set; }
        public string LocName { get; set; } = null!;

        public virtual BasicDetail Appli { get; set; } = null!;
        public virtual OptionMaster Option { get; set; } = null!;
    }
}
