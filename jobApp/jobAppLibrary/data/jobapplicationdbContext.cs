﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace jobApp.data
{
    public partial class jobapplicationdbContext : DbContext
    {
        public jobapplicationdbContext()
        {
        }

        public jobapplicationdbContext(DbContextOptions<jobapplicationdbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<BasicDetail> BasicDetails { get; set; } = null!;
        public virtual DbSet<EduDetail> EduDetails { get; set; } = null!;
        public virtual DbSet<LanguageMapping> LanguageMappings { get; set; } = null!;
        public virtual DbSet<LocationMapping> LocationMappings { get; set; } = null!;
        public virtual DbSet<OptionMaster> OptionMasters { get; set; } = null!;
        public virtual DbSet<PreferenceContact> PreferenceContacts { get; set; } = null!;
        public virtual DbSet<ReferenceContact> ReferenceContacts { get; set; } = null!;
        public virtual DbSet<SelectMaster> SelectMasters { get; set; } = null!;
        public virtual DbSet<TechnologyMapping> TechnologyMappings { get; set; } = null!;
        public virtual DbSet<WorkExperience> WorkExperiences { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySql("server=localhost;database=jobapplicationdb;user=root", Microsoft.EntityFrameworkCore.ServerVersion.Parse("10.4.22-mariadb"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("utf8mb4_general_ci")
                .HasCharSet("utf8mb4");

            modelBuilder.Entity<BasicDetail>(entity =>
            {
                entity.HasKey(e => e.AppliId)
                    .HasName("PRIMARY");

                entity.ToTable("basic_details");

                entity.Property(e => e.AppliId)
                    .HasColumnType("int(11)")
                    .HasColumnName("appli_id");

                entity.Property(e => e.Addr1)
                    .HasMaxLength(50)
                    .HasColumnName("addr1");

                entity.Property(e => e.Addr2)
                    .HasMaxLength(50)
                    .HasColumnName("addr2");

                entity.Property(e => e.City)
                    .HasMaxLength(50)
                    .HasColumnName("city");

                entity.Property(e => e.Designation)
                    .HasMaxLength(70)
                    .HasColumnName("designation");

                entity.Property(e => e.Dob)
                    .HasColumnType("datetime")
                    .HasColumnName("DOB");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .HasColumnName("email");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(50)
                    .HasColumnName("first_name");

                entity.Property(e => e.Gender)
                    .HasMaxLength(10)
                    .HasColumnName("gender");

                entity.Property(e => e.LastName)
                    .HasMaxLength(50)
                    .HasColumnName("last_name");

                entity.Property(e => e.PhoneNo)
                    .HasMaxLength(50)
                    .HasColumnName("phone_no");

                entity.Property(e => e.Relationship)
                    .HasMaxLength(10)
                    .HasColumnName("relationship");

                entity.Property(e => e.State)
                    .HasMaxLength(20)
                    .HasColumnName("state");

                entity.Property(e => e.Zipcode)
                    .HasMaxLength(20)
                    .HasColumnName("zipcode");
            });

            modelBuilder.Entity<EduDetail>(entity =>
            {
                entity.HasKey(e => e.EduId)
                    .HasName("PRIMARY");

                entity.ToTable("edu_details");

                entity.HasIndex(e => e.AppliId, "appli_id");

                entity.HasIndex(e => e.CourseId, "course_id");

                entity.Property(e => e.EduId)
                    .HasColumnType("int(11)")
                    .HasColumnName("edu_id");

                entity.Property(e => e.AppliId)
                    .HasColumnType("int(11)")
                    .HasColumnName("appli_id");

                entity.Property(e => e.CourseId)
                    .HasColumnType("int(11)")
                    .HasColumnName("course_id");

                entity.Property(e => e.NameOfBoard)
                    .HasMaxLength(70)
                    .HasColumnName("name_of_board");

                entity.Property(e => e.NameOfUni)
                    .HasMaxLength(20)
                    .HasColumnName("name_of_uni");

                entity.Property(e => e.PassYear)
                    .HasMaxLength(10)
                    .HasColumnName("pass_year");

                entity.Property(e => e.Percentage)
                    .HasColumnType("int(11)")
                    .HasColumnName("percentage");

                entity.HasOne(d => d.Appli)
                    .WithMany(p => p.EduDetails)
                    .HasForeignKey(d => d.AppliId)
                    .HasConstraintName("edu_details_ibfk_1");

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.EduDetails)
                    .HasForeignKey(d => d.CourseId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("edu_details_ibfk_2");
            });

            modelBuilder.Entity<LanguageMapping>(entity =>
            {
                entity.HasKey(e => e.LangId)
                    .HasName("PRIMARY");

                entity.ToTable("language_mapping");

                entity.HasIndex(e => e.AppliId, "appli_id");

                entity.HasIndex(e => e.OptionId, "option_id");

                entity.Property(e => e.LangId)
                    .HasColumnType("int(11)")
                    .HasColumnName("lang_id");

                entity.Property(e => e.AppliId)
                    .HasColumnType("int(11)")
                    .HasColumnName("appli_id");

                entity.Property(e => e.LangName)
                    .HasMaxLength(30)
                    .HasColumnName("lang_name");

                entity.Property(e => e.OptionId)
                    .HasColumnType("int(11)")
                    .HasColumnName("option_id");

                entity.HasOne(d => d.Appli)
                    .WithMany(p => p.LanguageMappings)
                    .HasForeignKey(d => d.AppliId)
                    .HasConstraintName("language_mapping_ibfk_2");

                entity.HasOne(d => d.Option)
                    .WithMany(p => p.LanguageMappings)
                    .HasForeignKey(d => d.OptionId)
                    .HasConstraintName("language_mapping_ibfk_1");
            });

            modelBuilder.Entity<LocationMapping>(entity =>
            {
                entity.HasKey(e => e.LocId)
                    .HasName("PRIMARY");

                entity.ToTable("location_mapping");

                entity.HasIndex(e => e.AppliId, "location_mapping_ibfk_1");

                entity.HasIndex(e => e.OptionId, "option_id");

                entity.Property(e => e.LocId)
                    .HasColumnType("int(11)")
                    .HasColumnName("loc_id");

                entity.Property(e => e.AppliId)
                    .HasColumnType("int(11)")
                    .HasColumnName("appli_id");

                entity.Property(e => e.LocName)
                    .HasMaxLength(30)
                    .HasColumnName("loc_name");

                entity.Property(e => e.OptionId)
                    .HasColumnType("int(11)")
                    .HasColumnName("option_id");

                entity.HasOne(d => d.Appli)
                    .WithMany(p => p.LocationMappings)
                    .HasForeignKey(d => d.AppliId)
                    .HasConstraintName("location_mapping_ibfk_1");

                entity.HasOne(d => d.Option)
                    .WithMany(p => p.LocationMappings)
                    .HasForeignKey(d => d.OptionId)
                    .HasConstraintName("location_mapping_ibfk_2");
            });

            modelBuilder.Entity<OptionMaster>(entity =>
            {
                entity.HasKey(e => e.OptionId)
                    .HasName("PRIMARY");

                entity.ToTable("option_master");

                entity.HasIndex(e => e.SelectId, "select_id");

                entity.Property(e => e.OptionId)
                    .HasColumnType("int(11)")
                    .HasColumnName("option_id");

                entity.Property(e => e.Name)
                    .HasMaxLength(30)
                    .HasColumnName("name");

                entity.Property(e => e.SelectId)
                    .HasColumnType("int(11)")
                    .HasColumnName("select_id");

                entity.Property(e => e.Val)
                    .HasMaxLength(30)
                    .HasColumnName("val");

                entity.HasOne(d => d.Select)
                    .WithMany(p => p.OptionMasters)
                    .HasForeignKey(d => d.SelectId)
                    .HasConstraintName("option_master_ibfk_1");
            });

            modelBuilder.Entity<PreferenceContact>(entity =>
            {
                entity.HasKey(e => e.PrefId)
                    .HasName("PRIMARY");

                entity.ToTable("preference_contact");

                entity.HasIndex(e => e.AppliId, "appli_id");

                entity.HasIndex(e => e.DeptId, "dept_id");

                entity.HasIndex(e => e.LocId, "loc_id");

                entity.Property(e => e.PrefId)
                    .HasColumnType("int(11)")
                    .HasColumnName("pref_id");

                entity.Property(e => e.AppliId)
                    .HasColumnType("int(11)")
                    .HasColumnName("appli_id");

                entity.Property(e => e.CurrCtc)
                    .HasMaxLength(50)
                    .HasColumnName("curr_CTC");

                entity.Property(e => e.DeptId)
                    .HasColumnType("int(11)")
                    .HasColumnName("dept_id");

                entity.Property(e => e.ExpCtc)
                    .HasMaxLength(50)
                    .HasColumnName("exp_CTC");

                entity.Property(e => e.LocId)
                    .HasColumnType("int(11)")
                    .HasColumnName("loc_id");

                entity.Property(e => e.NoticePeriod)
                    .HasMaxLength(30)
                    .HasColumnName("notice_period");

                entity.HasOne(d => d.Appli)
                    .WithMany(p => p.PreferenceContacts)
                    .HasForeignKey(d => d.AppliId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("preference_contact_ibfk_2");

                entity.HasOne(d => d.Dept)
                    .WithMany(p => p.PreferenceContactDepts)
                    .HasForeignKey(d => d.DeptId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("preference_contact_ibfk_1");

                entity.HasOne(d => d.Loc)
                    .WithMany(p => p.PreferenceContactLocs)
                    .HasForeignKey(d => d.LocId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("preference_contact_ibfk_3");
            });

            modelBuilder.Entity<ReferenceContact>(entity =>
            {
                entity.HasKey(e => e.RefId)
                    .HasName("PRIMARY");

                entity.ToTable("reference_contact");

                entity.HasIndex(e => e.AppliId, "appli_id");

                entity.Property(e => e.RefId)
                    .HasColumnType("int(11)")
                    .HasColumnName("ref_id");

                entity.Property(e => e.AppliId)
                    .HasColumnType("int(11)")
                    .HasColumnName("appli_id");

                entity.Property(e => e.RefContact)
                    .HasMaxLength(30)
                    .HasColumnName("ref_contact");

                entity.Property(e => e.RefName)
                    .HasMaxLength(50)
                    .HasColumnName("ref_name");

                entity.Property(e => e.RefRelation)
                    .HasMaxLength(50)
                    .HasColumnName("ref_relation");

                entity.HasOne(d => d.Appli)
                    .WithMany(p => p.ReferenceContacts)
                    .HasForeignKey(d => d.AppliId)
                    .HasConstraintName("reference_contact_ibfk_1");
            });

            modelBuilder.Entity<SelectMaster>(entity =>
            {
                entity.HasKey(e => e.SelectId)
                    .HasName("PRIMARY");

                entity.ToTable("select_master");

                entity.Property(e => e.SelectId)
                    .HasColumnType("int(11)")
                    .HasColumnName("select_id");

                entity.Property(e => e.Desc)
                    .HasMaxLength(30)
                    .HasColumnName("desc");

                entity.Property(e => e.Name)
                    .HasMaxLength(30)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<TechnologyMapping>(entity =>
            {
                entity.HasKey(e => e.TechId)
                    .HasName("PRIMARY");

                entity.ToTable("technology_mapping");

                entity.HasIndex(e => e.AppliId, "appli_id");

                entity.HasIndex(e => e.OptionId, "option_id");

                entity.Property(e => e.TechId)
                    .HasColumnType("int(11)")
                    .HasColumnName("tech_id");

                entity.Property(e => e.AppliId)
                    .HasColumnType("int(11)")
                    .HasColumnName("appli_id");

                entity.Property(e => e.OptionId)
                    .HasColumnType("int(11)")
                    .HasColumnName("option_id");

                entity.Property(e => e.TechName)
                    .HasMaxLength(30)
                    .HasColumnName("tech_name");

                entity.HasOne(d => d.Appli)
                    .WithMany(p => p.TechnologyMappings)
                    .HasForeignKey(d => d.AppliId)
                    .HasConstraintName("technology_mapping_ibfk_1");

                entity.HasOne(d => d.Option)
                    .WithMany(p => p.TechnologyMappings)
                    .HasForeignKey(d => d.OptionId)
                    .HasConstraintName("technology_mapping_ibfk_2");
            });

            modelBuilder.Entity<WorkExperience>(entity =>
            {
                entity.HasKey(e => e.WorkId)
                    .HasName("PRIMARY");

                entity.ToTable("work_experience");

                entity.HasIndex(e => e.AppliId, "appli_id");

                entity.Property(e => e.WorkId)
                    .HasColumnType("int(11)")
                    .HasColumnName("work_id");

                entity.Property(e => e.AppliId)
                    .HasColumnType("int(11)")
                    .HasColumnName("appli_id");

                entity.Property(e => e.CompanyName)
                    .HasMaxLength(50)
                    .HasColumnName("company_name");

                entity.Property(e => e.Designation)
                    .HasMaxLength(100)
                    .HasColumnName("designation");

                entity.Property(e => e.FromD)
                    .HasColumnType("datetime")
                    .HasColumnName("from_d");

                entity.Property(e => e.ToD)
                    .HasColumnType("datetime")
                    .HasColumnName("to_d");

                entity.HasOne(d => d.Appli)
                    .WithMany(p => p.WorkExperiences)
                    .HasForeignKey(d => d.AppliId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("work_experience_ibfk_1");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
