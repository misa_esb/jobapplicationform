﻿using System;
using System.Collections.Generic;

namespace jobApp.data
{
    public partial class SelectMaster
    {
        public SelectMaster()
        {
            OptionMasters = new HashSet<OptionMaster>();
        }

        public int SelectId { get; set; }
        public string Name { get; set; } = null!;
        public string Desc { get; set; } = null!;

        public virtual ICollection<OptionMaster> OptionMasters { get; set; }
    }
}
