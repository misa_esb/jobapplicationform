﻿using System;
using System.Collections.Generic;

namespace jobApp.data
{
    public partial class TechnologyMapping
    {
        public int TechId { get; set; }
        public int OptionId { get; set; }
        public int AppliId { get; set; }
        public string TechName { get; set; } = null!;

        public virtual BasicDetail Appli { get; set; } = null!;
        public virtual OptionMaster Option { get; set; } = null!;
    }
}
