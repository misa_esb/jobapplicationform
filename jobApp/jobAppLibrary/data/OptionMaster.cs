﻿using System;
using System.Collections.Generic;

namespace jobApp.data
{
    public partial class OptionMaster
    {
        public OptionMaster()
        {
            EduDetails = new HashSet<EduDetail>();
            LanguageMappings = new HashSet<LanguageMapping>();
            LocationMappings = new HashSet<LocationMapping>();
            PreferenceContactDepts = new HashSet<PreferenceContact>();
            PreferenceContactLocs = new HashSet<PreferenceContact>();
            TechnologyMappings = new HashSet<TechnologyMapping>();
        }

        public int OptionId { get; set; }
        public int SelectId { get; set; }
        public string Name { get; set; } = null!;
        public string Val { get; set; } = null!;

        public virtual SelectMaster Select { get; set; } = null!;
        public virtual ICollection<EduDetail> EduDetails { get; set; }
        public virtual ICollection<LanguageMapping> LanguageMappings { get; set; }
        public virtual ICollection<LocationMapping> LocationMappings { get; set; }
        public virtual ICollection<PreferenceContact> PreferenceContactDepts { get; set; }
        public virtual ICollection<PreferenceContact> PreferenceContactLocs { get; set; }
        public virtual ICollection<TechnologyMapping> TechnologyMappings { get; set; }
    }
}
