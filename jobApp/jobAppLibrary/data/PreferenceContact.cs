﻿using System;
using System.Collections.Generic;

namespace jobApp.data
{
    public partial class PreferenceContact
    {
        public int PrefId { get; set; }
        public int? AppliId { get; set; }
        public int? LocId { get; set; }
        public int? DeptId { get; set; }
        public string? NoticePeriod { get; set; }
        public string? ExpCtc { get; set; }
        public string? CurrCtc { get; set; }

        public virtual BasicDetail? Appli { get; set; }
        public virtual OptionMaster? Dept { get; set; }
        public virtual OptionMaster? Loc { get; set; }
    }
}
