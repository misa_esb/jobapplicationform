﻿using System;
using System.Collections.Generic;

namespace jobApp.data
{
    public partial class ReferenceContact
    {
        public int RefId { get; set; }
        public int? AppliId { get; set; }
        public string? RefName { get; set; }
        public string? RefContact { get; set; }
        public string? RefRelation { get; set; }

        public virtual BasicDetail? Appli { get; set; }
    }
}
