﻿using System;
using System.Collections.Generic;

namespace jobApp.data
{
    public partial class BasicDetail
    {
        public BasicDetail()
        {
            EduDetails = new HashSet<EduDetail>();
            LanguageMappings = new HashSet<LanguageMapping>();
            LocationMappings = new HashSet<LocationMapping>();
            PreferenceContacts = new HashSet<PreferenceContact>();
            ReferenceContacts = new HashSet<ReferenceContact>();
            TechnologyMappings = new HashSet<TechnologyMapping>();
            WorkExperiences = new HashSet<WorkExperience>();
        }

        public int AppliId { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Designation { get; set; }
        public string? Addr1 { get; set; }
        public string? Addr2 { get; set; }
        public string? Email { get; set; }
        public string? City { get; set; }
        public string? PhoneNo { get; set; }
        public string? State { get; set; }
        public string? Gender { get; set; }
        public string? Zipcode { get; set; }
        public string? Relationship { get; set; }
        public DateTime? Dob { get; set; }

        public virtual ICollection<EduDetail> EduDetails { get; set; }
        public virtual ICollection<LanguageMapping> LanguageMappings { get; set; }
        public virtual ICollection<LocationMapping> LocationMappings { get; set; }
        public virtual ICollection<PreferenceContact> PreferenceContacts { get; set; }
        public virtual ICollection<ReferenceContact> ReferenceContacts { get; set; }
        public virtual ICollection<TechnologyMapping> TechnologyMappings { get; set; }
        public virtual ICollection<WorkExperience> WorkExperiences { get; set; }
    }
}
