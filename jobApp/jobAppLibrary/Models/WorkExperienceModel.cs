﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jobAppLibrary.Models
{
    public class WorkExperienceModel
    {
        public int WorkId { get; set; }
        public int? AppliId { get; set; }
        public string? CompanyName { get; set; }
        public string? Designation { get; set; }
        public DateTime? FromD { get; set; }
        public DateTime? ToD { get; set; }
    }
}
