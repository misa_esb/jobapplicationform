﻿global using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;

namespace jobAppLibrary.Models
{
    public  class BasicDetailModel
    {     
        public int AppliId { get; set; }

        [Display(Name = "First Name")]
        [Required]
        public string? FirstName { get; set; }
        [Display(Name = "Last Name")]
        [Required]
        public string? LastName { get; set; }
        public string? Designation { get; set; }

        [Display(Name = "address")]
        [Required]
        public string? Addr1 { get; set; }

        [Display(Name = "second address")]
        public string? Addr2 { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "email address")]
        [Required]
        public string? Email { get; set; }

        [Required]
        public string? City { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone Number")]
        [Required]
        public string? PhoneNo { get; set; }

        [Required]
        public string? State { get; set; }

        [Required]
        public string? Gender { get; set; }

        [Required]
        public string? Zipcode { get; set; }

        [Required]
        public string? Relationship { get; set; }

        [Display(Name = "date of birth")]
        [DataType(DataType.DateTime)]
        [Required]
        public DateTime? Dob { get; set; }

        public List<EduDetailModel> EduDetails { get; set; }
        public List<PreferenceContactModel> PreferenceContacts{ get; set; }
        public List<ReferenceContactModel> ReferenceContacts { get; set; }
        public List<WorkExperienceModel> WorkExperiences { get; set; }

        public List<LanguageMappingModel> languages { get; set; }

        public List<TechnologyMappingModel> techs { get; set; }


    }
}
