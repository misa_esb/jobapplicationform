﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jobAppLibrary.Models
{
    public class LanguageMappingModel
    {
        public int LangId { get; set; }
        public int OptionId { get; set; }
        public int AppliId { get; set; }
        public string LangName { get; set; } = null!;
    }
}
