﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jobAppLibrary.Models
{
    public class EduDetailModel
    {
        public int EduId { get; set; }

        [Required]
        public int? CourseId { get; set; }

        [Display(Name = "board name")]
        [Required]
        public string? NameOfBoard { get; set; }

        [Display(Name = "uni name")]
        [Required]
        public string? NameOfUni { get; set; }

        [Display(Name = "pass year")]
        [Required]
        public string? PassYear { get; set; }

        [Display(Name = "percentage")]
        [Required]
        public int Percentage { get; set; }

        public OptionMaster course { get; set; }

    }
}
