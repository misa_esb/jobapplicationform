﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jobAppLibrary.Models
{
    public class TechnologyMappingModel
    {
        public int TechId { get; set; }
        public int OptionId { get; set; }
        public int AppliId { get; set; }
        public string TechName { get; set; } = null!;
    }
}
