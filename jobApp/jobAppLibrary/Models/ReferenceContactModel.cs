﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jobAppLibrary.Models
{
    public class ReferenceContactModel
    {
        public int RefId { get; set; }
        public int? AppliId { get; set; }
        public string? RefName { get; set; }
        public string? RefContact { get; set; }
        public string? RefRelation { get; set; }
    }
}
