﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jobAppLibrary.Models
{
    public class LocationMappingModel
    {
        public int LocId { get; set; }
        public int AppliId { get; set; }
        public int OptionId { get; set; }
        public string LocName { get; set; } = null!;
    }
}
