﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jobAppLibrary.Models
{
    public class PreferenceContactModel
    {

        public int PrefId { get; set; }
        public int? AppliId { get; set; }
        public int? LocId { get; set; }
        public int? DeptId { get; set; }
        public string? NoticePeriod { get; set; }
        public string? ExpCtc { get; set; }
        public string? CurrCtc { get; set; }


    }
}
