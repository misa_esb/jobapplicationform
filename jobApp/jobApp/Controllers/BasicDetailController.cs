﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using jobAppLibrary.Repositories;
using jobAppLibrary.Models;
using jobApp.data;

namespace jobApp.Controllers
{
    [Route("api/jobApp")]
    [ApiController]
    public class BasicDetailController : ControllerBase
    {
        
        private readonly IbasicDetailsRepository _basicDetailsRepository;
        private readonly IEduDetailsRepository _eduDetailsRepository;
        public BasicDetailController(IbasicDetailsRepository BasicDetailsRepository,IEduDetailsRepository eduDetailsRepository)
        {
            _basicDetailsRepository = BasicDetailsRepository;
            _eduDetailsRepository = eduDetailsRepository;
        }

        [HttpGet("ViewEmployeeDetails/{id:int:min(1)}")]
        public async Task<BasicDetailModel> ViewEmployeeDetails(int id)
        {
            var data = await _basicDetailsRepository.get(id);
            return data;

        }

        [HttpGet("DeleteEmployee/{id:int:min(1)}")]

        public async Task<bool> DeleteEmployee(int id)
        {
            var result = await _basicDetailsRepository.delete(id);
            if (result == true)
            {
                return true;
            }
            return false;
        }

        [HttpGet("addEmployee")]
        public async Task<BasicDetailModel> addEmployeeForm()
        {
            var model = new BasicDetailModel();
            return model;

        }


        [HttpPost("addEmployee")]
        public async Task<int> addEmployee([FromBody]BasicDetailModel model)
        {
            if (ModelState.IsValid)
            {
                int id = await _basicDetailsRepository.Add(model);
                return id;
            }
            else
            {
                return 0;
            }
        }


        [HttpPost("updateEmployee")]
        public async Task<bool> updateEmployee([FromBody] BasicDetailModel model)
        {
            if (ModelState.IsValid)
            {
                return await _basicDetailsRepository.update(model);
               
            }
            else
            {
                return false;
            }
        }

        [HttpGet("getAllEmployees")]
        public async Task<IEnumerable<BasicDetailModel>> GetAllBooks()
        {
            var data = await _basicDetailsRepository.GetAll();
            return data;
        }






        [HttpGet("getAllDetails")]
        public async Task<List<BasicDetail>> GetAllDetails()
        {
            var data = await _eduDetailsRepository.GetAll();
            return data;
        }
    }
}
